
PKGLIST += nasm@2.15.05

.PHONY: nasm@2.15.05

nasm@2.15.05: nasm/2.15.05/bin/nasm ~/modulefiles/nasm/2.15.05

~/tars/nasm-2.15.05.tar.xz: | ~/tars
	$(WGET) https://www.nasm.us/pub/nasm/releasebuilds/2.15.05/nasm-2.15.05.tar.xz -O nasm-2.15.05.tar.xz
	mv nasm-2.15.05.tar.xz ~/tars/nasm-2.15.05.tar.xz

nasm/2.15.05/src/configure: ~/tars/nasm-2.15.05.tar.xz
	rm -fr nasm/2.15.05; mkdir -p nasm/2.15.05; cd nasm/2.15.05
	untar $< -m
	rm -rf src; mv nasm-2.15.05 src

nasm/2.15.05/src/Makefile: nasm/2.15.05/src/configure
	cd nasm/2.15.05/src
	./configure --prefix=$(PWD)/nasm/2.15.05

nasm/2.15.05/bin/nasm: nasm/2.15.05/src/Makefile
	$(MAKE) -C nasm/2.15.05/src install -j $$N_JOBS

~/modulefiles/nasm/2.15.05:
	mkdir -p ~/modulefiles/nasm
	cat > $@ << EOF
	#%Module1.0#
	
	conflict nasm
	
	prepend-path PATH               $$HOME/got/nasm/2.15.05/bin
	prepend-path MANPATH            $$HOME/got/nasm/2.15.05/share/man
	EOF


