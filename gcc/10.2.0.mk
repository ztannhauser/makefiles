
PKGLIST += gcc@10.2.0

.PHONY: gcc@10.2.0

gcc@10.2.0: gcc/10.2.0/bin/gcc ~/modulefiles/gcc/10.2.0

~/tars/gcc-10.2.0.tar.xz: | ~/tars
	$(WGET) https://ftp.gnu.org/gnu/gcc/gcc-10.2.0/gcc-10.2.0.tar.xz -O gcc-10.2.0.tar.xz
	mv gcc-10.2.0.tar.xz ~/tars/gcc-10.2.0.tar.xz

gcc/10.2.0/src/configure: ~/tars/gcc-10.2.0.tar.xz
	mkdir -p gcc/10.2.0; cd gcc/10.2.0
	untar $< -m
	rm -fr src; mv gcc-10.2.0 src
	cd src
	sed -i.orig '/m64=/s/lib64/lib/' gcc/config/i386/t-linux64

gcc/10.2.0/build/Makefile: gcc/10.2.0/src/configure | gmp@6.2.1 mpfr@4.1.0 mpc@1.2.1 flex@2.6.4 bison@3.7.6
	cd gcc/10.2.0; mkdir -p build; cd build
	. ~/init/sh; module load $|
	../src/configure --prefix=$(PWD)/gcc/10.2.0 \
		 --with-system-zlib --disable-multilib --enable-languages=c,c++

gcc/10.2.0/bin/gcc: gcc/10.2.0/build/Makefile | gmp@6.2.1 mpfr@4.1.0 mpc@1.2.1 flex@2.6.4 bison@3.7.6
	. ~/init/sh; module load $|
	$(MAKE) -C gcc/10.2.0/build -j $$N_JOBS # these have to be seperate
	$(MAKE) -C gcc/10.2.0/build install

~/modulefiles/gcc/10.2.0:
	mkdir -p ~/modulefiles/gcc
	cat > $@ << EOF
	#%Module1.0#
	
	conflict gcc
	
	module load gmp@6.2.1 mpfr@4.1.0 mpc@1.2.1
	
	prepend-path PATH               $$HOME/got/gcc/10.2.0/bin
	prepend-path C_INCLUDE_PATH     $$HOME/got/gcc/10.2.0/include
	prepend-path CPLUS_INCLUDE_PATH $$HOME/got/gcc/10.2.0/include
	prepend-path LIBRARY_PATH       $$HOME/got/gcc/10.2.0/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/gcc/10.2.0/lib
	prepend-path PKG_CONFIG_PATH    $$HOME/got/gcc/10.2.0/lib/pkgconfig
	EOF
