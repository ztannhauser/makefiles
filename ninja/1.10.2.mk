
PKGLIST += ninja@1.10.2

.PHONY: ninja@1.10.2

ninja@1.10.2: ninja/1.10.2/bin/ninja ~/modulefiles/ninja/1.10.2

~/tars/ninja-1.10.2.tar.gz: | ~/tars
	$(WGET) https://github.com/ninja-build/ninja/archive/refs/tags/v1.10.2.tar.gz -O ninja-1.10.2.tar.gz
	mv ninja-1.10.2.tar.gz ~/tars/ninja-1.10.2.tar.gz

ninja/1.10.2/src/configure.py: ~/tars/ninja-1.10.2.tar.gz
	rm -fr ninja/1.10.2; mkdir -p ninja/1.10.2; cd ninja/1.10.2
	untar $< -m
	rm -rf src; mv ninja-1.10.2 src

ninja/1.10.2/src/ninja: ninja/1.10.2/src/configure.py | python@2.7.18
	cd ninja/1.10.2/src;
	. ~/init/sh; module load $|
	python ./configure.py --bootstrap

ninja/1.10.2/bin/ninja: ninja/1.10.2/src/ninja
	cd ninja/1.10.2;
	install src/ninja -D bin/ninja
	install src/misc/bash-completion -D ./share/bash-completion/completions/ninja

~/modulefiles/ninja/1.10.2:
	mkdir -p ~/modulefiles/ninja
	cat > $@ << EOF
	#%Module1.0#
	
	conflict ninja
	
	prepend-path PATH                     $$HOME/got/ninja/1.10.2/bin
	prepend-path BASH_COMPLETION_USER_DIR $$HOME/got/ninja/1.10.2/share/bash-completion

	EOF

