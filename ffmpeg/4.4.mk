
PKGLIST += ffmpeg@4.4

.PHONY: ffmpeg@4.4

ffmpeg@4.4: ffmpeg/4.4/bin/ffmpeg ~/modulefiles/ffmpeg/4.4

~/tars/ffmpeg-4.4.tar.xz: | ~/tars
	$(WGET) https://ffmpeg.org/releases/ffmpeg-4.4.tar.xz -O ffmpeg-4.4.tar.xz
	mv ffmpeg-4.4.tar.xz ~/tars/ffmpeg-4.4.tar.xz

ffmpeg/4.4/src/configure: ~/tars/ffmpeg-4.4.tar.xz
	rm -fr ffmpeg/4.4; mkdir -p ffmpeg/4.4; cd ffmpeg/4.4
	untar $< -m
	rm -rf src; mv ffmpeg-4.4 src

ffmpeg/4.4/build/Makefile: ffmpeg/4.4/src/configure | yasm@1.3.0 x264@20210211 x265@3.4
	cd ffmpeg/4.4; mkdir -p build; cd build
	. ~/init/sh; module load $|
	../src/configure --prefix=$(PWD)/ffmpeg/4.4 \
		--enable-libx265 --enable-libx264 \
		--enable-gpl --enable-nonfree --enable-shared

ffmpeg/4.4/bin/ffmpeg: ffmpeg/4.4/build/Makefile | x265@3.4 x264@20210211 yasm@1.3.0
	. ~/init/sh; module load $|
	$(MAKE) -C ffmpeg/4.4/build install -j $$N_JOBS

~/modulefiles/ffmpeg/4.4:
	mkdir -p ~/modulefiles/ffmpeg
	cat > $@ << EOF
	#%Module1.0#
	
	conflict ffmpeg
	
	module load x265@3.4 x264@20210211
	
	prepend-path PATH               $$HOME/got/ffmpeg/4.4/bin
	prepend-path C_INCLUDE_PATH     $$HOME/got/ffmpeg/4.4/include
	prepend-path CPLUS_INCLUDE_PATH $$HOME/got/ffmpeg/4.4/include
	prepend-path LIBRARY_PATH       $$HOME/got/ffmpeg/4.4/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/ffmpeg/4.4/lib
	prepend-path MANPATH            $$HOME/got/ffmpeg/4.4/share/man
	prepend-path PKG_CONFIG_PATH    $$HOME/got/ffmpeg/4.4/lib/pkgconfig
	EOF

