
PKGLIST += yasm@1.3.0

.PHONY: yasm@1.3.0

yasm@1.3.0: yasm/1.3.0/bin/yasm ~/modulefiles/yasm/1.3.0

~/tars/yasm-1.3.0.tar.gz: | ~/tars
	$(WGET) https://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz -O yasm-1.3.0.tar.gz
	mv yasm-1.3.0.tar.gz ~/tars/yasm-1.3.0.tar.gz

yasm/1.3.0/src/configure: ~/tars/yasm-1.3.0.tar.gz
	rm -fr yasm/1.3.0; mkdir -p yasm/1.3.0; cd yasm/1.3.0
	untar $< -m
	rm -rf src; mv yasm-1.3.0 src
	cd src; sed -i 's#) ytasm.*#)#' Makefile.in

yasm/1.3.0/build/Makefile: yasm/1.3.0/src/configure
	cd yasm/1.3.0; mkdir -p build; cd build
	../src/configure --prefix=$(PWD)/yasm/1.3.0

yasm/1.3.0/bin/yasm: yasm/1.3.0/build/Makefile
	$(MAKE) -C yasm/1.3.0/build install -j $$N_JOBS

~/modulefiles/yasm/1.3.0:
	mkdir -p ~/modulefiles/yasm
	cat > $@ << EOF
	#%Module1.0#
	
	conflict yasm
	
	prepend-path PATH               $$HOME/got/yasm/1.3.0/bin
	prepend-path C_INCLUDE_PATH     $$HOME/got/yasm/1.3.0/include
	prepend-path CPLUS_INCLUDE_PATH $$HOME/got/yasm/1.3.0/include
	prepend-path LIBRARY_PATH       $$HOME/got/yasm/1.3.0/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/yasm/1.3.0/lib
	prepend-path MANPATH            $$HOME/got/yasm/1.3.0/share/man
	EOF


