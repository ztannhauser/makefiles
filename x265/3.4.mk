
PKGLIST += x265@3.4

.PHONY: x265@3.4

x265@3.4: x265/3.4/lib/libx265.so ~/modulefiles/x265/3.4

~/tars/x265-3.4.tar.gz: | ~/tars
	$(WGET) http://anduin.linuxfromscratch.org/BLFS/x265/x265_3.4.tar.gz -O x265-3.4.tar.gz
	mv x265-3.4.tar.gz ~/tars/x265-3.4.tar.gz

x265/3.4/src/readme.rst: ~/tars/x265-3.4.tar.gz
	rm -fr x265/3.4; mkdir -p x265/3.4; cd x265/3.4
	untar $< -m
	rm -rf src; mv x265_3.4 src

x265/3.4/build/Makefile: x265/3.4/src/readme.rst | cmake@3.20.1
	set -e
	cd x265/3.4; mkdir -p build; cd build
	. ~/init/sh; module load $|
	cmake -D CMAKE_INSTALL_PREFIX=$(PWD)/x265/3.4 ../src/source

x265/3.4/lib/libx265.so: x265/3.4/build/Makefile
	$(MAKE) -C x265/3.4/build install -j $$N_JOBS

~/modulefiles/x265/3.4:
	mkdir -p ~/modulefiles/x265
	cat > $@ << EOF
	#%Module1.0#
	
	conflict x265
	
	prepend-path PATH               $$HOME/got/x265/3.4/bin
	prepend-path C_INCLUDE_PATH     $$HOME/got/x265/3.4/include
	prepend-path CPLUS_INCLUDE_PATH $$HOME/got/x265/3.4/include
	prepend-path LIBRARY_PATH       $$HOME/got/x265/3.4/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/x265/3.4/lib
	prepend-path PKG_CONFIG_PATH    $$HOME/got/x265/3.4/lib/pkgconfig
	EOF


