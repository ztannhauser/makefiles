
PKGLIST += cmake@3.20.1

.PHONY: cmake@3.20.1

cmake@3.20.1: cmake/3.20.1/bin/cmake ~/modulefiles/cmake/3.20.1

~/tars/cmake-3.20.1.tar.gz: | ~/tars
	$(WGET) https://cmake.org/files/v3.20/cmake-3.20.1.tar.gz -O cmake-3.20.1.tar.gz
	mv cmake-3.20.1.tar.gz ~/tars/cmake-3.20.1.tar.gz

cmake/3.20.1/src/configure: ~/tars/cmake-3.20.1.tar.gz
	rm -fr cmake/3.20.1; mkdir -p cmake/3.20.1; cd cmake/3.20.1
	untar $< -m
	rm -rf src; mv cmake-3.20.1 src
	cd src; sed -i '/"lib64"/s/64//' Modules/GNUInstallDirs.cmake

cmake/3.20.1/build/Makefile: cmake/3.20.1/src/configure
	cd cmake/3.20.1; mkdir -p build; cd build
	../src/bootstrap --prefix=$(PWD)/cmake/3.20.1 --parallel=$$N_JOBS

cmake/3.20.1/bin/cmake: cmake/3.20.1/build/Makefile
	$(MAKE) -C cmake/3.20.1/build install -j $$N_JOBS

~/modulefiles/cmake/3.20.1:
	mkdir -p ~/modulefiles/cmake
	cat > $@ << EOF
	#%Module1.0#
	
	conflict cmake
	
	prepend-path PATH               $$HOME/got/cmake/3.20.1/bin
	prepend-path C_INCLUDE_PATH     $$HOME/got/cmake/3.20.1/include
	prepend-path CPLUS_INCLUDE_PATH $$HOME/got/cmake/3.20.1/include
	prepend-path LIBRARY_PATH       $$HOME/got/cmake/3.20.1/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/cmake/3.20.1/lib
	prepend-path PKG_CONFIG_PATH    $$HOME/got/cmake/3.20.1/lib/pkgconfig
	prepend-path MANPATH            $$HOME/got/cmake/3.20.1/share/man
	EOF

