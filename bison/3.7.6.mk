
PKGLIST += bison@3.7.6

.PHONY: bison@3.7.6

bison@3.7.6: bison/3.7.6/bin/bison ~/modulefiles/bison/3.7.6

~/tars/bison-3.7.6.tar.xz: | ~/tars
	$(WGET) https://ftp.gnu.org/gnu/bison/bison-3.7.6.tar.xz -O bison-3.7.6.tar.xz
	mv bison-3.7.6.tar.xz ~/tars/bison-3.7.6.tar.xz

bison/3.7.6/src/configure: ~/tars/bison-3.7.6.tar.xz
	mkdir -p bison/3.7.6; cd bison/3.7.6
	untar $< -m
	rm -rf src; mv bison-3.7.6 src

bison/3.7.6/build/Makefile: bison/3.7.6/src/configure
	cd bison/3.7.6; mkdir -p build; cd build
	../src/configure --prefix=$(PWD)/bison/3.7.6 --parallel=$$N_JOBS

bison/3.7.6/bin/bison: bison/3.7.6/build/Makefile
	$(MAKE) -C bison/3.7.6/build install -j $$N_JOBS

~/modulefiles/bison/3.7.6:
	mkdir -p ~/modulefiles/bison
	cat > $@ << EOF
	#%Module1.0#
	
	conflict bison
	
	prepend-path PATH               $$HOME/got/bison/3.7.6/bin
	prepend-path LIBRARY_PATH       $$HOME/got/bison/3.7.6/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/bison/3.7.6/lib
	prepend-path MANPATH            $$HOME/got/bison/3.7.6/share/man
	EOF


