
PKGLIST += gmp@6.2.1

.PHONY: gmp@6.2.1

gmp@6.2.1: gmp/6.2.1/lib/libgmp.so ~/modulefiles/gmp/6.2.1

~/tars/gmp-6.2.1.tar.lz: | ~/tars
	$(WGET) https://ftp.gnu.org/gnu/gmp/gmp-6.2.1.tar.lz -O gmp-6.2.1.tar.lz
	mv gmp-6.2.1.tar.lz ~/tars/gmp-6.2.1.tar.lz

gmp/6.2.1/src/configure: ~/tars/gmp-6.2.1.tar.lz
	mkdir -p gmp/6.2.1; cd gmp/6.2.1
	untar $< -m
	rm src -rf; mv gmp-6.2.1 src

gmp/6.2.1/build/Makefile: gmp/6.2.1/src/configure
	cd gmp/6.2.1; mkdir -p build; cd build
	../src/configure --prefix=$(PWD)/gmp/6.2.1 --parallel=$$N_JOBS \
		 --enable-cxx

gmp/6.2.1/lib/libgmp.so: gmp/6.2.1/build/Makefile
	$(MAKE) -C gmp/6.2.1/build install -j $$N_JOBS

~/modulefiles/gmp/6.2.1:
	mkdir -p ~/modulefiles/gmp
	cat > $@ << EOF
	#%Module1.0#
	
	conflict gmp
	
	prepend-path C_INCLUDE_PATH     $$HOME/got/gmp/6.2.1/include
	prepend-path CPLUS_INCLUDE_PATH $$HOME/got/gmp/6.2.1/include
	prepend-path LIBRARY_PATH       $$HOME/got/gmp/6.2.1/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/gmp/6.2.1/lib
	prepend-path PKG_CONFIG_PATH    $$HOME/got/gmp/6.2.1/lib/pkgconfig
	EOF

