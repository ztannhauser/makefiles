
PKGLIST += bazelisk@1.7.5

.PHONY: bazelisk@1.7.5

bazelisk@1.7.5: bazelisk/1.7.5/bin/bazelisk ~/modulefiles/bazelisk/1.7.5

~/tars/bazelisk-1.7.5.tar.gz: | ~/tars
	$(WGET) https://github.com/bazelbuild/bazelisk/archive/refs/tags/v1.7.5.tar.gz -O bazelisk-1.7.5.tar.gz
	mv bazelisk-1.7.5.tar.gz ~/tars/bazelisk-1.7.5.tar.gz

bazelisk/1.7.5/src/bazelisk.go: ~/tars/bazelisk-1.7.5.tar.gz
	rm -fr bazelisk/1.7.5; mkdir -p bazelisk/1.7.5; cd bazelisk/1.7.5
	untar $< -m
	rm -rf src; mv bazelisk-1.7.5 src

bazelisk/1.7.5/src/bazelisk: bazelisk/1.7.5/src/bazelisk.go | go@1.16.3
	cd bazelisk/1.7.5/src
	. ~/init/sh; module load $|
	go build bazelisk.go

bazelisk/1.7.5/bin/bazelisk: bazelisk/1.7.5/src/bazelisk
	cd bazelisk/1.7.5
	install src/bazelisk -D bin/bazelisk

~/modulefiles/bazelisk/1.7.5:
	mkdir -p ~/modulefiles/bazelisk
	cat > $@ << EOF
	#%Module1.0#
	
	conflict bazelisk
	
	prepend-path PATH  $$HOME/got/bazelisk/1.7.5/bin
	EOF

