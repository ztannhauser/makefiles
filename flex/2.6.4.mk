
PKGLIST += flex@2.6.4

.PHONY: flex@2.6.4

flex@2.6.4: flex/2.6.4/bin/flex ~/modulefiles/flex/2.6.4

~/tars/flex-2.6.4.tar.gz: | ~/tars
	$(WGET) https://github.com/westes/flex/releases/download/v2.6.4/flex-2.6.4.tar.gz -O flex-2.6.4.tar.gz
	mv flex-2.6.4.tar.gz ~/tars/flex-2.6.4.tar.gz

flex/2.6.4/src/configure: ~/tars/flex-2.6.4.tar.gz
	mkdir -p flex/2.6.4; cd flex/2.6.4
	untar $< -m
	rm -rf src; mv flex-2.6.4 src

flex/2.6.4/build/Makefile: flex/2.6.4/src/configure
	cd flex/2.6.4; mkdir -p build; cd build
	../src/configure --prefix=$(PWD)/flex/2.6.4

flex/2.6.4/bin/flex: flex/2.6.4/build/Makefile
	$(MAKE) -C flex/2.6.4/build install -j $$N_JOBS

~/modulefiles/flex/2.6.4:
	mkdir -p ~/modulefiles/flex
	cat > $@ << EOF
	#%Module1.0#
	
	conflict flex
	
	prepend-path PATH               $$HOME/got/flex/2.6.4/bin
	prepend-path C_INCLUDE_PATH     $$HOME/got/flex/2.6.4/include
	prepend-path CPLUS_INCLUDE_PATH $$HOME/got/flex/2.6.4/include
	prepend-path LIBRARY_PATH       $$HOME/got/flex/2.6.4/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/flex/2.6.4/lib
	prepend-path PKG_CONFIG_PATH    $$HOME/got/flex/2.6.4/share/man
	EOF

