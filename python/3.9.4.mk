
PKGLIST += python@3.9.4

.PHONY: python@3.9.4

python@3.9.4: python/3.9.4/bin/python ~/modulefiles/python/3.9.4

~/tars/python-3.9.4.tar.xz: | ~/tars
	$(WGET) https://www.python.org/ftp/python/3.9.4/Python-3.9.4.tar.xz -O python-3.9.4.tar.xz
	mv python-3.9.4.tar.xz ~/tars/python-3.9.4.tar.xz

python/3.9.4/src/configure: ~/tars/python-3.9.4.tar.xz
	rm -fr python/3.9.4; mkdir -p python/3.9.4; cd python/3.9.4
	untar $< -m
	rm -rf src; mv Python-3.9.4 src

python/3.9.4/build/Makefile: python/3.9.4/src/configure
	cd python/3.9.4; mkdir -p build; cd build
	../src/configure --prefix=$(PWD)/python/3.9.4 \
		--enable-optimizations

python/3.9.4/bin/python: python/3.9.4/build/Makefile
	$(MAKE) -C python/3.9.4/build install -j $$N_JOBS
	cd python/3.9.4/bin; ln -s python3 python

~/modulefiles/python/3.9.4:
	mkdir -p ~/modulefiles/python
	cat > $@ << EOF
	#%Module1.0#
	
	conflict python
	
	prepend-path PATH               $$HOME/got/python/3.9.4/bin
	prepend-path C_INCLUDE_PATH     $$HOME/got/python/3.9.4/include
	prepend-path CPLUS_INCLUDE_PATH $$HOME/got/python/3.9.4/include
	prepend-path LIBRARY_PATH       $$HOME/got/python/3.9.4/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/python/3.9.4/lib
	prepend-path PKG_CONFIG_PATH    $$HOME/got/python/3.9.4/lib/pkgconfig
	prepend-path MANPATH            $$HOME/got/python/3.9.4/share/man
	EOF


