
PKGLIST += python@2.7.18

.PHONY: python@2.7.18

python@2.7.18: python/2.7.18/bin/python ~/modulefiles/python/2.7.18

~/tars/python-2.7.18.tar.xz: | ~/tars
	$(WGET) https://www.python.org/ftp/python/2.7.18/Python-2.7.18.tar.xz -O python-2.7.18.tar.xz
	mv python-2.7.18.tar.xz ~/tars/python-2.7.18.tar.xz

python/2.7.18/src/configure: ~/tars/python-2.7.18.tar.xz
	rm -fr python/2.7.18; mkdir -p python/2.7.18; cd python/2.7.18
	untar $< -m
	rm -rf src; mv Python-2.7.18 src

python/2.7.18/build/Makefile: python/2.7.18/src/configure
	cd python/2.7.18; mkdir -p build; cd build
	../src/configure --prefix=$(PWD)/python/2.7.18 \
		--enable-unicode=ucs4 --enable-shared --enable-optimizations

python/2.7.18/bin/python: python/2.7.18/build/Makefile
	$(MAKE) -C python/2.7.18/build install -j $$N_JOBS

~/modulefiles/python/2.7.18:
	mkdir -p ~/modulefiles/python
	cat > $@ << EOF
	#%Module1.0#
	
	conflict python
	
	prepend-path PATH               $$HOME/got/python/2.7.18/bin
	prepend-path C_INCLUDE_PATH     $$HOME/got/python/2.7.18/include
	prepend-path CPLUS_INCLUDE_PATH $$HOME/got/python/2.7.18/include
	prepend-path LIBRARY_PATH       $$HOME/got/python/2.7.18/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/python/2.7.18/lib
	prepend-path MANPATH            $$HOME/got/python/2.7.18/share/man
	prepend-path PKG_CONFIG_PATH    $$HOME/got/python/2.7.18/lib/pkgconfig
	EOF


