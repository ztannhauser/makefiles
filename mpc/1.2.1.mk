
PKGLIST += mpc@1.2.1

.PHONY: mpc@1.2.1

mpc@1.2.1: mpc/1.2.1/lib/libmpc.so ~/modulefiles/mpc/1.2.1

~/tars/mpc-1.2.1.tar.gz: | ~/tars
	$(WGET) https://ftp.gnu.org/gnu/mpc/mpc-1.2.1.tar.gz -O mpc-1.2.1.tar.gz
	mv mpc-1.2.1.tar.gz ~/tars/mpc-1.2.1.tar.gz

mpc/1.2.1/src/configure: ~/tars/mpc-1.2.1.tar.gz
	mkdir -p mpc/1.2.1; cd mpc/1.2.1
	untar $< -m
	rm -rf src; mv mpc-1.2.1 src

mpc/1.2.1/build/Makefile: mpc/1.2.1/src/configure | mpfr@4.1.0 gmp@6.2.1
	cd mpc/1.2.1; mkdir -p build; cd build
	. ~/init/sh; module load $|
	../src/configure --prefix=$(PWD)/mpc/1.2.1 --parallel=$$N_JOBS

mpc/1.2.1/lib/libmpc.so: mpc/1.2.1/build/Makefile | mpfr@4.1.0 gmp@6.2.1
	. ~/init/sh; module load $|
	$(MAKE) -C mpc/1.2.1/build install -j $$N_JOBS

~/modulefiles/mpc/1.2.1:
	mkdir -p ~/modulefiles/mpc
	cat > $@ << EOF
	#%Module1.0#
	
	conflict mpc
	
	module load mpfr@4.1.0 gmp@6.2.1
	
	prepend-path C_INCLUDE_PATH     $$HOME/got/mpc/1.2.1/include
	prepend-path CPLUS_INCLUDE_PATH $$HOME/got/mpc/1.2.1/include
	prepend-path LIBRARY_PATH       $$HOME/got/mpc/1.2.1/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/mpc/1.2.1/lib
	prepend-path PKG_CONFIG_PATH    $$HOME/got/mpc/1.2.1/lib/pkgconfig
	EOF


