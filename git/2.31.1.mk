
PKGLIST += git@2.31.1

.PHONY: git@2.31.1

git@2.31.1: git/2.31.1/bin/git ~/modulefiles/git/2.31.1

~/tars/git-2.31.1.tar.xz: | ~/tars
	$(WGET) https://www.kernel.org/pub/software/scm/git/git-2.31.1.tar.xz -O git-2.31.1.tar.xz
	mv git-2.31.1.tar.xz ~/tars/git-2.31.1.tar.xz

git/2.31.1/src/configure: ~/tars/git-2.31.1.tar.xz
	rm -fr git/2.31.1; mkdir -p git/2.31.1; cd git/2.31.1
	untar $< -m
	rm -rf src; mv git-2.31.1 src

git/2.31.1/src/config.mak.autogen: git/2.31.1/src/configure | curl@7.76.1
	cd git/2.31.1/src;
	. ~/init/sh; module load $|
	./configure --prefix=$(PWD)/git/2.31.1 \
		--with-gitconfig=/etc/gitconfig --with-curl

git/2.31.1/bin/git: git/2.31.1/src/config.mak.autogen | curl@7.76.1
	. ~/init/sh; module load $|
	$(MAKE) -C git/2.31.1/src install -j $$N_JOBS

~/modulefiles/git/2.31.1:
	mkdir -p ~/modulefiles/git
	cat > $@ << EOF
	#%Module1.0#
	module load curl@7.76.1
	conflict git
	prepend-path PATH $$HOME/got/git/2.31.1/bin
	EOF


