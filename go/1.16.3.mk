
PKGLIST += go@1.16.3

.PHONY: go@1.16.3

go@1.16.3: go/1.16.3/bin/go ~/modulefiles/go/1.16.3

~/tars/go-1.16.3.tar.gz: | ~/tars
	$(WGET) https://golang.org/dl/go1.16.3.linux-amd64.tar.gz -O go-1.16.3.tar.gz
	mv go-1.16.3.tar.gz ~/tars/go-1.16.3.tar.gz

go/1.16.3/src/bin/go: ~/tars/go-1.16.3.tar.gz
	rm -fr go/1.16.3; mkdir -p go/1.16.3; cd go/1.16.3
	untar $< -m
	rm -rf src; mv go src

go/1.16.3/bin/go: go/1.16.3/src/bin/go
	cd go/1.16.3
	install src/bin/go -D bin/go

~/modulefiles/go/1.16.3:
	mkdir -p ~/modulefiles/go
	cat > $@ << EOF
	#%Module1.0#
	
	conflict go
	
	prepend-path PATH   $$HOME/got/go/1.16.3/bin
	prepend-path GOROOT $$HOME/got/go/1.16.3/src
	EOF

