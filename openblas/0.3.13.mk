
PKGLIST += openblas@0.3.13

.PHONY: openblas@0.3.13

openblas@0.3.13: openblas/0.3.13/lib/libopenblas.a ~/modulefiles/openblas/0.3.13

~/tars/openblas-0.3.13.tar.gz: | ~/tars
	$(WGET) https://github.com/xianyi/OpenBLAS/releases/download/v0.3.13/OpenBLAS-0.3.13.tar.gz -O openblas-0.3.13.tar.gz
	mv openblas-0.3.13.tar.gz ~/tars/openblas-0.3.13.tar.gz

openblas/0.3.13/src/Makefile: ~/tars/openblas-0.3.13.tar.gz
	rm -fr openblas/0.3.13; mkdir -p openblas/0.3.13; cd openblas/0.3.13
	untar $< -m
	rm -rf src; mv OpenBLAS-0.3.13 src

openblas/0.3.13/lib/libopenblas.a: openblas/0.3.13/src/Makefile
	cd openblas/0.3.13
	BINARY=64 make -C src -j $$N_JOBS # needs to be seperate
	BINARY=64 make -C src PREFIX=$$PWD install

~/modulefiles/openblas/0.3.13:
	mkdir -p ~/modulefiles/openblas
	cat > $@ << EOF
	#%Module1.0#
	
	conflict openblas
	
	prepend-path C_INCLUDE_PATH     $$HOME/got/openblas/0.3.13/include
	prepend-path CPLUS_INCLUDE_PATH $$HOME/got/openblas/0.3.13/include
	prepend-path LIBRARY_PATH       $$HOME/got/openblas/0.3.13/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/openblas/0.3.13/lib
	prepend-path PKG_CONFIG_PATH    $$HOME/got/openblas/0.3.13/lib/pkgconfig
	EOF


