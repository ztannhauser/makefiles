
PKGLIST += curl@7.76.1

.PHONY: curl@7.76.1

curl@7.76.1: curl/7.76.1/bin/curl ~/modulefiles/curl/7.76.1

~/tars/curl-7.76.1.tar.xz: | ~/tars
	$(WGET) https://curl.haxx.se/download/curl-7.76.1.tar.xz -O curl-7.76.1.tar.xz
	mv curl-7.76.1.tar.xz ~/tars/curl-7.76.1.tar.xz

curl/7.76.1/src/configure: ~/tars/curl-7.76.1.tar.xz
	rm -fr curl/7.76.1; mkdir -p curl/7.76.1; cd curl/7.76.1
	untar $< -m
	rm -rf src; mv curl-7.76.1 src
	cd src; grep -rl '#!.*python$$' | xargs sed -i '1s/python/&3/'

curl/7.76.1/build/Makefile: curl/7.76.1/src/configure
	cd curl/7.76.1; mkdir -p build; cd build
	../src/configure --prefix=$(PWD)/curl/7.76.1 \
		--enable-threaded-resolver --with-ca-path=/etc/ssl/certs

curl/7.76.1/bin/curl: curl/7.76.1/build/Makefile
	$(MAKE) -C curl/7.76.1/build install -j $$N_JOBS

~/modulefiles/curl/7.76.1:
	mkdir -p ~/modulefiles/curl
	cat > $@ << EOF
	#%Module1.0#
	conflict curl
	prepend-path PATH               $$HOME/got/curl/7.76.1/bin
	prepend-path C_INCLUDE_PATH     $$HOME/got/curl/7.76.1/include
	prepend-path CPLUS_INCLUDE_PATH $$HOME/got/curl/7.76.1/include
	prepend-path LIBRARY_PATH       $$HOME/got/curl/7.76.1/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/curl/7.76.1/lib
	prepend-path MANPATH            $$HOME/got/curl/7.76.1/share/man
	prepend-path PKG_CONFIG_PATH    $$HOME/got/curl/7.76.1/lib/pkgconfig
	EOF


