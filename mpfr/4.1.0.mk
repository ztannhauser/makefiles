
PKGLIST += mpfr@4.1.0

.PHONY: mpfr@4.1.0

mpfr@4.1.0: mpfr/4.1.0/lib/libmpfr.so ~/modulefiles/mpfr/4.1.0

~/tars/mpfr-4.1.0.tar.xz: | ~/tars
	$(WGET) https://www.mpfr.org/mpfr-current/mpfr-4.1.0.tar.xz -O mpfr-4.1.0.tar.xz
	mv mpfr-4.1.0.tar.xz ~/tars/mpfr-4.1.0.tar.xz

mpfr/4.1.0/src/configure: ~/tars/mpfr-4.1.0.tar.xz
	mkdir -p mpfr/4.1.0; cd mpfr/4.1.0
	untar $< -m
	rm -rf src; mv mpfr-4.1.0 src

mpfr/4.1.0/build/Makefile: mpfr/4.1.0/src/configure | gmp@6.2.1
	cd mpfr/4.1.0; mkdir -p build; cd build
	. ~/init/sh; module load $|
	../src/configure --prefix=$(PWD)/mpfr/4.1.0 --parallel=$$N_JOBS \
		--enable-thread-safe

mpfr/4.1.0/lib/libmpfr.so: mpfr/4.1.0/build/Makefile | gmp@6.2.1
	. ~/init/sh; module load $|
	$(MAKE) -C mpfr/4.1.0/build install -j $$N_JOBS

~/modulefiles/mpfr/4.1.0:
	mkdir -p ~/modulefiles/mpfr
	cat > $@ << EOF
	#%Module1.0#
	
	conflict mpfr
	
	module load gmp@6.2.1
	
	prepend-path C_INCLUDE_PATH     $$HOME/got/mpfr/4.1.0/include
	prepend-path CPLUS_INCLUDE_PATH $$HOME/got/mpfr/4.1.0/include
	prepend-path LIBRARY_PATH       $$HOME/got/mpfr/4.1.0/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/mpfr/4.1.0/lib
	prepend-path PKG_CONFIG_PATH    $$HOME/got/mpfr/4.1.0/lib/pkgconfig
	EOF

