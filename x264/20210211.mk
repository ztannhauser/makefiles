
PKGLIST += x264@20210211

.PHONY: x264@20210211

x264@20210211: x264/20210211/lib/libx264.so ~/modulefiles/x264/20210211

~/tars/x264-20210211.tar.xz: | ~/tars
	$(WGET) http://anduin.linuxfromscratch.org/BLFS/x264/x264-20210211.tar.xz -O x264-20210211.tar.xz
	mv x264-20210211.tar.xz ~/tars/x264-20210211.tar.xz

x264/20210211/src/configure: ~/tars/x264-20210211.tar.xz
	rm -fr x264/20210211; mkdir -p x264/20210211; cd x264/20210211
	untar $< -m
	rm -rf src; mv x264-20210211 src

x264/20210211/src/config.mak: x264/20210211/src/configure | nasm@2.15.05
	set -e
	cd x264/20210211/src
	. ~/init/sh; module load $|
	./configure --prefix=$(PWD)/x264/20210211 \
		--enable-shared --disable-cli

x264/20210211/lib/libx264.so: x264/20210211/src/config.mak | nasm@2.15.05
	. ~/init/sh; module load $|
	$(MAKE) -C x264/20210211/src install -j $$N_JOBS

~/modulefiles/x264/20210211:
	mkdir -p ~/modulefiles/x264
	cat > $@ << EOF
	#%Module1.0#
	
	conflict x264
	
	prepend-path PATH               $$HOME/got/x264/20210211/bin
	prepend-path C_INCLUDE_PATH     $$HOME/got/x264/20210211/include
	prepend-path CPLUS_INCLUDE_PATH $$HOME/got/x264/20210211/include
	prepend-path LIBRARY_PATH       $$HOME/got/x264/20210211/lib
	prepend-path LD_LIBRARY_PATH    $$HOME/got/x264/20210211/lib
	prepend-path PKG_CONFIG_PATH    $$HOME/got/x264/20210211/lib/pkgconfig
	EOF


